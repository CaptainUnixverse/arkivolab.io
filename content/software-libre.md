---
title: "Lista de aplicaciones de software libre"
cover: "images/gafam-arden.png"
date: "2020-04-30"
author: "arbok"
volverPrincipio: true
---

Alternativas libres a programas o plataformas privativas. 

> Nota: las aplicaciones móviles para Android se pueden instalar a través de [F-Droid](https://radioslibres.net/f-droid-aplicaciones-libres-para-tu-celular), una alternativa libre a la tienda por defecto de Android (Google Play Store).

Se utilizarán los siguientes símbolos para facilitar la lectura de la lista:
- {{< fontawesome mobile >}}: aplicación para móvil.
---
- {{< fontawesome laptop >}}: aplicación para ordenador de mesa/portátil.
---
- {{< fontawesome gas-pump >}}: servicio online (*[Software como Servicio](https://es.wikipedia.org/wiki/Software_como_servicio)*), no requiere instalar ningún programa y se accede a través del navegador.
---
- {{< fontawesome code >}}: código fuente del *software* para configurar un servidor propio. Aunque todas las opciones que se listan a continuación tienen su código accesible (propiedad intrínseca del software libre), este símbolo se utilizará para aquellas opciones que, en principio, no se encuentra un servicio online.
---
- {{< fontawesome fediverse >}}: plataformas que forman parte del **fediverso** (*universo federado*). El **fediverso** es una **red de redes sociales** libres y descentralizadas donde les usuaries pueden contactar entre elles independientemente de la red que utilicen (Mastodon, GNUSocial, Pleroma, PixelFed, PeerTube...). Al contrario de lo que ocurre con las redes sociales **centralizadas** y **privativas** (Facebook, Twitter, Instagram, etc) donde sus usuaries de distintas redes no pueden comunicarse entre sí. Por ejemplo, una usuaria de Facebook no puede comunicarse con una usuaria de Twitter.
	- Si quieres saber más sobre el fediverso puedes ver [esta charla](https://www.youtube.com/watch?v=V-iAyWmjUE&feature=youtu.be&t=101) (34 min.) o leer [este fanzine](https://documents.lleialtat.cat/LleialTec/Fediverse/textos/fediverse-xarxes-lliures-a4.pdf) (15 pág.).
---

## Índice

- [Redes sociales](#redes-sociales)
- [Streaming de vídeos & música](#streaming-de-vídeos--música)
- [Radio/Podcasts](#radio-&-podcasts)
- [Mensajería instantánea & videollamadas](#mensajería-instantánea--videollamadas)
- [Programas de edición](#programas-de-edición)
- [Pizarras online](#pizarras-online)
- [Sistemas operativos](#sistemas-operativos)
- [Navegadores web](#navegadores-web)
- [Buscadores web](#buscadores-web)
- [Redacción colaborativa](#redacción-colaborativa)
- [Correo electrónico](#correo-electrónico)
- [Compartir archivos](#compartir-archivos)
- [Calendario](#calendario)
- [Mapas](#mapas)
- [Encuestas](#encuestas)
- [Gestión de contraseñas](#gestión-de-contraseñas)
- [Traductor online](#traductor-online)
- [Repositorios de código](#repositorios-de-código)
- [Administración de proyectos](#administración-de-proyectos)
- [Nubes](#nubes)
- [Organización de colectivos](#organización-de-colectivos)
- [Plataformas de enseñanza](#enseñanza)

---

## Redes sociales 

- **Twitter**:
	- {{< fontawesome fediverse >}} [Mastodon](https://radioslibres.net/mastodon-mejor-tootear-que-tuitear/)

- **Instagram**:
	- {{< fontawesome fediverse >}} [PixelFed](https://pixelfed.org)

## Streaming de vídeos & música

- **Netflix**:
	- **Streaming**:
		- [playdede](https://playdede.com/login)
	- **Streaming de torrents** ([¿qué es un torrent?](https://www.invidio.us/watch?v=4wd7pP7VQkg) + [fanzine pirata](https://gitlab.com/arkivo/arkivo.gitlab.io/-/raw/master/files/fanzine-pirata.pdf)):
		- {{< fontawesome mobile >}}{{< fontawesome laptop >}} [Popcorntime](https://popcorntime.app)
		- {{< fontawesome code >}} [Peerflix](https://github.com/mafintosh/peerflix)

	- **Descargar torrents** (seguir [Guía de ~librenauta](https://torrent.copiona.com))

- **Spotify**:
	- {{< fontawesome fediverse >}} [FunkWhale](https://funkwhale.audio)

- **Youtube**:
	- {{< fontawesome fediverse >}} [PeerTube](https://joinpeertube.org/es/#what-is-peertube)

	- **Aplicaciones cliente para ver vídeos de Youtube**:
		- {{< fontawesome mobile >}} [NewPipe](https://newpipe.schabi.org) (permite acceder a contenido de SoundCloud y PeerTube)

		- **[Instancias](https://github.com/omarroth/invidious/wiki/Invidious-Instances) de [Invidious](https://github.com/omarroth/invidious)**:
			- {{< fontawesome gas-pump >}} [invidio.us](https://www.invidio.us)

## Radio

- {{< fontawesome code >}} [AzuraCast](https://www.azuracast.com/): ofrece un conjunto de herramientas para gestionar un programa de radio.
- https://icecast.org/

## Mensajería instantánea & videollamadas

- **Mensajería instantánea** (como Whatsapp):
	- {{< fontawesome mobile >}}{{< fontawesome laptop >}} [Telegram](https://f-droid.org/es/packages/org.telegram.messenger/) (solo cifra los chats secretos)
	- {{< fontawesome mobile >}}{{< fontawesome laptop >}} [Signal](https://signal.org/es/)
	- {{< fontawesome mobile >}} [Briar](https://briarproject.org)
	- {{< fontawesome laptop >}}{{< fontawesome mobile >}} [Element](https://element.io/get-started): es una aplicación basada en el estándar de Matrix. Si no te gusta esta aplicación, puedes utilziar otra que te guste más buscando en https://matrix.org/clients/
- **Multiconferencia de voz** (como Discord):
	- {{< fontawesome mobile >}}{{< fontawesome laptop >}} [Mumble](https://www.mumble.info)

- **Videollamadas** (como Skype):
	- **Servidores de Jitsi** (para videollamadas grupales)
		- {{< fontawesome gas-pump >}} https://meet.jit.si
		- {{< fontawesome gas-pump >}} https://meet.guifi.net
		- {{< fontawesome gas-pump >}} https://vc.autistici.org
		- {{< fontawesome gas-pump >}} https://meet.greenhost.net/
		- {{< fontawesome gas-pump >}} https://framatalk.org/
		- {{< fontawesome gas-pump >}} https://calls.disroot.org/

## Programas de edición

- **Edición de vectores gráficos** (como Adobe Illustrator):
	- {{< fontawesome laptop >}} [Inkscape](https://inkscape.org/es/)

- **Edición de imágenes** (como Adobe Photoshop):
	- {{< fontawesome laptop >}} [Gimp](https://www.gimp.org)
- **Creación 3D** (como Unity):
	- {{< fontawesome laptop >}} [Blender](https://www.blender.org)
- **Pintura e ilustración digital**:
	- {{< fontawesome laptop >}} [Krita](https://krita.org/es/)

- **Edición de vídeo**:
	- {{< fontawesome laptop >}} [Kdenlive](https://kdenlive.org/es/)
	- {{< fontawesome laptop >}} [Olive](https://olivevideoeditor.org)

- **Retransmisión/Grabación de pantalla**:
	- {{< fontawesome laptop >}} [OBS Studio](https://es.wikipedia.org/wiki/Open_Broadcaster_Software)

- **Edición de audio**:
	- {{< fontawesome laptop >}} [Audacity](https://audacityteam.org)
	- {{< fontawesome laptop >}} [Ardour](https://ardour.org)

- **Maquetación de páginas** (como Adobe InDesign)
	- {{< fontawesome laptop >}} [Scribus](https://www.scribus.net)

## Pizarras online

- {{< fontawesome gas-pump >}} [Pizarra](https://cryptpad.fr/whiteboard), de la colección de servicios ofrecidos por [cryptpad](https://cryptpad.fr)
- {{< fontawesome gas-pump >}} [Excalidraw](https://victorhckinthefreeworld.com/2020/04/30/excalidraw-es-una-pizarra-en-blanco-en-la-que-realizar-diagramas-dibujados-a-mano/) (pizarra en blanco enfocada en dibujar diagramas y bocetos, tiene una opción para participar colaborativamente con otras personas)
 
## Sistemas operativos

- **Windows**:
	- {{< fontawesome laptop >}} [Distribuciones de GNU/Linux](https://www.genbeta.com/linux/31-distribuciones-linux-para-elegir-bien-que-necesitas-1)

- **Android**:
	- {{< fontawesome mobile >}} [LineageOS](https://wiki.lineageos.org/devices/)
	- {{< fontawesome mobile >}} [PostMarketOS](https://wiki.postmarketos.org/wiki/Devices)
	- {{< fontawesome mobile >}} [Replicant](https://replicant.us/supported-devices.php)

## Navegadores web

*Alternativas a Google Chrome.*

- {{< fontawesome laptop >}} [Firefox](https://www.mozilla.org/es-ES/firefox/new/)
- {{< fontawesome laptop >}} [Falkon](https://www.falkon.org)
- {{< fontawesome laptop >}}{{< fontawesome mobile >}} [Tor](https://www.torproject.org/download/)
- {{< fontawesome mobile >}} [GNU IceCat](https://f-droid.org/es/packages/org.gnu.icecat/)
- {{< fontawesome mobile >}} [Firefox Klar](https://f-droid.org/es/packages/org.mozilla.klar/)

## Buscadores web 

*Alternativas al buscador de Google.*

- {{< fontawesome gas-pump >}} [DuckDuckGo](https://duckduckgo.com)
- Instancias de [SearX](https://searx.space):
	- {{< fontawesome gas-pump >}} [Trovu](https://trovu.komun.org)
	- {{< fontawesome gas-pump >}} [Disroot Searx](https://search.disroot.org)

- {{< fontawesome gas-pump >}} [CC Search](https://ccsearch.creativecommons.org) (permite buscar contenido con licencia Creative Commons)
- {{< fontawesome laptop >}} [YaCy](https://yacy.net) (motor de búsqueda descentralizado peer-to-peer)
 
## Redacción colaborativa

*Alternativas a Google Docs.*

- {{< fontawesome code >}} [CodiMD](https://github.com/hackmdio/codimd) (versión libre de HackMD)

- **Hojas de cálculo**:
	- {{< fontawesome gas-pump >}} https://calc.disroot.org
	- {{< fontawesome gas-pump >}} [Hoja de cálculo](https://cryptpad.fr/sheet/), de la colección de servicios ofrecidos por [cryptpad](https://cryptpad.fr)

- **Pads o "notas colaborativas"**:
	- {{< fontawesome gas-pump >}} https://pad.riseup.net (pads púlicos)
	- {{< fontawesome gas-pump >}} https://pad.elbinario.net (pads públicos)
	- {{< fontawesome gas-pump >}} https://pad.disroot.org
	- {{< fontawesome gas-pump >}} https://pad.kefir.red
	- {{< fontawesome gas-pump >}} https://antonieta.vedetas.org
	- {{< fontawesome gas-pump >}} [Pad](https://cryptpad.fr/pad) con opción de añadir una contraseña, de la colección de servicios ofrecidos por [cryptpad](https://cryptpad.fr)

## Correo electrónico 

- **Servicios de correo electrónico** (como Gmail):
	- {{< fontawesome gas-pump >}} [RiseUp](https://riseup.net/es/email)
	- {{< fontawesome gas-pump >}} [Disroot](https://mail.disroot.org) (registrarse [aquí](https://user.disroot.org/pwm/public/newuser))
	- {{< fontawesome gas-pump >}} [ProtonMail](https://protonmail.com) (cuenta gratuita tiene un límite de 500MB de almacenamiento y 150 mensajes por día)
	- {{< fontawesome gas-pump >}} [Tutanota](https://tutanota.com/es/)

- **Gestor de correo electrónico**: permite gestionar varias cuentas de correo de distintos servicios en una misma aplicación
	- {{< fontawesome laptop >}} [Thunderbird](https://www.thunderbird.net/es-ES/)
	- {{< fontawesome mobile >}} [k-9 Mail](https://k9mail.app/)

## Compartir archivos

- {{< fontawesome laptop >}} [OnionShare](https://onionshare.org) (sin límite)
- {{< fontawesome gas-pump >}} [RiseupShare](https://share.riseup.net) (1GB máx.)
- {{< fontawesome gas-pump >}} [Lufi](https://upload.disroot.org) (1GB max.)
- {{< fontawesome gas-pump >}} [Firefox Send](https://send.firefox.com) (1GB máx.)

## Calendario

*Alternativas a Google Calendar.*

- {{< fontawesome mobile >}} [Calendario Simple](https://f-droid.org/es/packages/com.simplemobiletools.calendar.pro/) (calendario local, aunque permite sincronización con [NextCloud](https://nextcloud.com) y otros servicios que utilicen CalDav)
- {{< fontawesome laptop >}} Calendario integrado con el gestor de correo electrónico [Thunderbird](https://www.thunderbird.net/es-ES/).
- Calendarios de eventos públicos:
	- {{< fontawesome code >}} [Gancio](https://gancio.org)

## Mapas 

*Alternativas a Google Maps.*

- {{< fontawesome gas-pump >}} [OpenStreetMap](https://www.openstreetmap.org/)
- {{< fontawesome mobile >}} [OsmAnd](https://f-droid.org/es/packages/net.osmand.plus/)
- {{< fontawesome gas-pump >}} [uMap](https://umap.openstreetmap.fr/es/): permite crear un mapa colaborativo utilizando OpenStreeMap.

## Encuestas

- {{< fontawesome gas-pump >}} https://framadate.org
- {{< fontawesome gas-pump >}} https://poll.disroot.org
- {{< fontawesome gas-pump >}} [Encuesta](https://cryptpad.fr/poll), de la colección de servicios ofrecidos por [cryptpad](https://cryptpad.fr)

## Gestión de contraseñas

- {{< fontawesome laptop >}} [KeePassXC](https://keepassxc.org)
- {{< fontawesome mobile >}} [KeePassDX](https://www.keepassdx.com/)
- {{< fontawesome mobile >}} [KeePassDroid](https://f-droid.org/es/packages/com.android.keepass/)

## Traductor online

*Alternativas al Traductor de Google.*

- {{< fontawesome gas-pump >}} {{< fontawesome laptop >}} [DeepL](https://www.deepl.com)

## Repositorios de código 

*Alternativas a GitLab y GitHub.*

- {{< fontawesome gas-pump >}} [Gitea](https://git.disroot.org/explore/repos)
- {{< fontawesome gas-pump >}} [0xacab](https://0xacab.org/explore)

## Administración de proyectos 

*Alternativas a Trello.*

- {{< fontawesome gas-pump >}} [Kanban](https://cryptpad.fr/kanban), de la colección de servicios ofrecidos por [cryptpad](https://cryptpad.fr)
- {{< fontawesome gas-pump >}} [Taiga](https://taiga.io/)
- {{< fontawesome code >}} [Wekan](https://wekan.github.io)

## Nubes

*Alternativas a Google Drive o Dropbox.*

- [Cryptpad Drive](https://cryptpad.fr/drive/) (máx. 1GB), de la colección de servicios ofrecidos por [cryptpad](https://cryptpad.fr)
- Instancias de [Nextcloud](https://nextcloud.com):
	- {{< fontawesome gas-pump >}} [Disroot Cloud](https://cloud.disroot.org): registrarse [aquí](https://user.disroot.org/pwm/public/newuser) (máx. 2GB)

## Organización de colectivos

- {{< fontawesome gas-pump >}} [Crabgrass](https://crabgrass.riseup.net)

## Enseñanza

*Alternativas a Google [GSuite](https://arkivo.gitlab.io/posts/google-fuera/)*:
- LMS (Sistema de Gestión de Aprendizaje):
	- {{< fontawesome code >}} [Moodle](https://moodle.org/): Moodle 4.0 viene con BigBlueButton integrado
	- {{< fontawesome code >}} [Chamilo](https://chamilo.org/es/chamilo/): se puede integrar con BigBluButton
- {{< fontawesome code >}} [BigBlueButton](https://bigbluebutton.org/): plataforma de conferencias para la enseñanza online
