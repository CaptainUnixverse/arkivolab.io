+++
title = "Queremos a Google fuera de las escuelas"
date = "2020-05-02"
author = "arbok"
tags = ["anti-gooogle", "artículo", "software libre"]
cover = "images/gxxgle.png"
description = "**Google Suit for Education (GSuit)** es una *suite* de herramientas para Centros Educativos que se ha ido introduciendo en multidud de colegios. La multinacional está extendiendo su poder de vigilancia y control a otros ámbitos que antes eran inaccesibles, marcando un paso más hacia la privatización de las instituciones públicas."
showFullContent = false
volverPrincipio = true
+++

## Contexto

**Google Suit for Education (GSuite)** es una *suite* de herramientas para Centros Educativos que incluye:
- **Classroom**: permite al profesorado crear deberes y comunicarse con el alumnado.
- **Hangouts Meet**: permite al profesorado conectarse con el alumnado virtualmente a través de videollamada y mensajes.
- **Groups**: crear y participar en foros de clase.
- **Drive**: almacenar y organizar deberes, documentos o el programa de la clase.
- **Gmail**: permite configurar un sistema de correo electrónico para toda la escuela o intercambiar correos electrónicos entre el profesorado y el alumnado.
- **Calendar**: permite crear un calendario conjunto entre el profesorado y el alumnado o la institución.
- **Vault**: permite al profesorado agregar alumnas/os y administrar dispositivos.

Actualmente, **GSuite** es utilizado por multitud de colegios en España.

## Problemática

En el momento en el que Google se introduce en las escuelas, la empresa pasa a **custodiar en sus servidores datos que antes eran inaccesibles**. Todos estos datos generados a partir de la actividad del estudiantado en su *suite* de aplicaciones permite leer conversaciones personales, búsquedas en Internet, programaciones de las clases y calendarios de actividades, es decir, conocer al detalle qué usos hacen les menores de la tecnología, alcanzando ámbitos privados de les menores y pudiendo así, crear perfiles psicológicos.

Todos estos datos, a pesar de que Google "promete" que no los venderá con fines comerciales, ha demostrado constantemente que este es el pilar de negocio de la multinacional y que ha roto su "promesa" en reiteradas ocasiones. Sin ir más lejos, **en Estados Unidos se le ha acusado a Google de "espiar" a millones de niñes**  (noticia [aquí](https://kaosenlared.net/escandalo-en-google-asi-espia-a-millones-de-ninos-en-el-colegio-y-en-su-casa/), febrero 2020).

Google ha encontrado un nuevo mercado en la información de las escuelas, y esto es preocupante sobre todo porque les menores aún no han adquirido una visión crítica y se encuentran en el proceso de creación de unos hábitos en los que se sustentarán en el futuro. 

Ante esta situación, la intromisión de la multinacional en las escuelas tendrá, de cara al futuro, un impacto importante:
- **Creación de una dependencia tecnológica** a los programas de Google por parte de les estudiantes y profesores, fortaleciendo así su monopolio tecnológico y sus ganancias, en contraposición a los valores que se deberían fomentar: el conocimiento libre, [software libre](https://www.gnu.org/philosophy/free-sw.es.html), pensamiento crítico y neutralidad tecnológica, sin favorecer una tecnología sobre otra.
- **Endo-privatización de la educación**, es decir, la delegación a agentes privados funciones públicas relacionadas con el proceso educativo.
- **Imposición de la lógica de Google**, y por ende, la lógica neoliberal.
- **Todes les niñes que hayan usado desde primaria GSuite, habrán registrado un rastro identificable en la red** de toda su actividad, lo que suscita la pregunta: ¿qué sucede con los perfiles creados por Google cuando les estudiantes dejan la escuela?

Esta situación es una **consecuencia directa de unas políticas de austeridad** que han debilitado el sistema educativo. Los recortes en la educación y la sobrecarga del profesorado ha debilitado el sistema y a otras alternativas más libres, generando el caldo de cultivo perfecto para asimilar el sistema de Google.

## GSuite para Centros Educativos, ¿cuánto cuesta?

GSuite para Centros Educativos es **gratuito** y además, ofrece almacenamiento ilimitado, tal y como explica Google en su página:

{{< image src="/images/dif.png" alt="Diferencias entre GSuite para Centros Educativos y GSuite Basic" position="center" style="border-radius: 8px;" >}}

**El hecho de que la suite de Google sea gratuita es muy preocupante**, ya que no solo demuestra el interés que tiene por el "mercado" que ha encontrado en las instituciones educativas, sino que los beneficios que obtendrá serán mucho mayores, **siendo les niñes el producto real de su negocio, concretamente sus datos**.

## Movilización en Catalunya contra GSuite

La plataforma [Xnet](https://xnet-x.net), la cual trabaja por los derechos digitales, democracia en red y la libertad de expresión,  comenzado negociaciones con la Generalitat de Catalunya con varias propuestas y soluciones (actualizaciones de la negociación [aquí](https://xnet-x.net/no-autorizar-google-suite-escuelas/)).

Según el [artículo](https://xnet-x.net/no-autorizar-google-suite-escuelas/) publicado por Xnet en septiembre de 2019, las acciones que proponen para las madres y padres son **no firmar las autorizaciones para utilizar GSuite** en las escuelas o **firmarla añadiendo la siguiente cláusula**:

---

*Doy el consentimiento a la alta de mi hija / mi hijo a las herramientas de la Suite Educativa de Google únicamente de forma PROVISIONAL hasta que se implemente otra opción donde no se pierda el control sobre sus datos y no se pueda rastrear su comportamiento.*

---

La introducción de GSuite en los centros relega a un segundo plano otras herramientas de [software libre](https://www.gnu.org/philosophy/free-sw.es.htm) que ya se utilizaban, como [Moodle](https://moodle.org) o [Chamilo](https://chamilo.org/es/). El artículo lanza la siguiente pregunta respecto a esto:

*¿Por qué se abandona un modelo, también gratuito, que podría modificarse según las exigencias, más distribuido, transparente y verificable en relación a la privacidad y la protección de datos del alumnado, por otro modelo centralizado, cerrado y poco respetuoso con los datos de las usuarias?*

## Conclusión

Existen alternativas a GSuite que, además ya estaban en uso y en lugar de potenciarlas y mejorarlas, las comunidades y los centros ceden ante la expansión comercial de la multinacional. Si tu centro está utilizando GSuite, infórmate sobre alguna campaña que ya esté en marcha en tu zona y organízate con otras madres y padres.



