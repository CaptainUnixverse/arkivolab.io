+++
title = "Lista de aplicaciones de software libre"
date = "2020-05-05"
author = "arbok"
tags = ["recursos", "software libre", "alternativas"]
cover = "images/gafam-arden.png"
redirectURL = "software-libre"
description = "Alternativas libres a programas o plataformas privativas | *Ilustración de [Péhä](https://lesptitsdessinsdepeha.wordpress.com).*"
showFullContent = false
+++


